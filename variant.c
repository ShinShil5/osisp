#include <stdio.h>

void main(int argc, char ** argv) {
	unsigned long long int passportNum, resVariant;
	int varsAmount;
	printf("Enter the passport num: ");
	scanf("%llu",&passportNum);
	printf("Enter the vars amount: ");
	scanf("%d", &varsAmount);
	resVariant = (passportNum * passportNum) % varsAmount + 1;
	printf("Your variant: %llu\n", resVariant);
}