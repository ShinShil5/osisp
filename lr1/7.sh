#!/bin/bash

name="main"
filename="main.c"
exe="main.exe"

until [ "$1" = "" ]; do
    case $1 in
        -o | --output ) shift; exe=$1 ;;
        -h | --help ) usage; exit ;;
        * ) filename=$1 ;;
    esac
    shift
done

testFileName=${filename%.c}
if [ $testFileName = $filename ]; then
    filename="$filename.c"
fi
if [ "$exe" = "main.exe" ];  then
    exe="$testFileName.exe"
fi

testExe=${exe%.exe}

if [ $testExe = $exe ]; then
    echo -en "\nBad output filename: $exe\n\n"
    exit 1
fi

error=$((gcc $filename -o $exe) 2>&1)
if [ "$?"="0" ]; then
    $exe
else
    echo -en "\nErrors during compile\n\n"
    echo $error
fi

#echo "C - $filename, EXE - $exe"
#echo "ERROR: $error"