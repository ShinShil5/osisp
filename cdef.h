#ifndef CDEF_H
#define CDEF_H

#define STRCMP_EQUAL 0
#define STRCMP_FIRST_BIGGER 1
#define STRCMP_FIRST_LOWER -1
#define STRCMP_SECOD_LOWER 1
#define STRCMP_SECOND_BIGGER -1
#define FORK_RESULT_ERROR -1
#define FORK_RESULT_CHILD 0


typedef enum _BOOL {
    FALSE = 0,
    TRUE = 1
} BOOL;

typedef unsigned int UINT;

//Linux defines
// typedef struct dirent DIRECORY_ENTRY, *PDIRECORY_ENTRY;
// typedef DIR DIRECTORY, *PDIRECTORY; 
#endif