#ifndef CMDARGS_H
#define CMDARGS_H

#include "../cdef.h"
#include "../logger/logger.h"
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

typedef struct stat STAT, * PSTAT;
typedef struct dirent DIRECTORY_ENTRY, *PDIRECTORY_ENTRY;
typedef DIR DIRECTORY, *PDIRECTORY; 

extern char* errorMessage;
typedef enum _ArgType {
    UINTEGER = 0,
    FOLDER = 1, 
    VALIDFILE = 2,
    STRING = 3
} ARG_TYPE;

//needArgc = argc - 1
BOOL checkArgs(int argc, char** argv, int needArgc, ...);
BOOL checkArg(char* arg, ARG_TYPE type);
BOOL folder(char* arg);
BOOL uinteger(char* arg);
BOOL validfile(char* arg);
void cleanErrorMessage();
#endif