#include "cmdargs.h"

char* errorMessage = NULL;

void setErrorMessage(char* message) {
    if(errorMessage != NULL) {
        free(errorMessage);
    }
    errorMessage = (char*)malloc(sizeof(char) * strlen(message) + 1);
    strcpy(errorMessage, message);
}

BOOL checkArgs(int argc, char **argv, int needArgc, ...)
{
    if (needArgc != argc - 1)
    {
        logerror("Wrong amount of arguments - %d, shoud be - %d",argc - 1, needArgc);
        
        return FALSE;
    }
    va_list argsTypes;
    va_start(argsTypes, needArgc);
    for (int i = 1; i < argc; ++i) //1st - programmName
    {
        ARG_TYPE argType = va_arg(argsTypes, ARG_TYPE);
        if (checkArg(argv[i], argType) == FALSE)
        {
            logerror("Error at %d param: %s", i + 1, argv[i]);
            return FALSE;
        }
    }
    va_end(argsTypes);
    return TRUE;
}

BOOL checkArg(char *arg, ARG_TYPE type)
{
    if (arg == NULL)
        return FALSE;
    switch (type)
    {
    case UINTEGER:
        return uinteger(arg);
    case FOLDER:
        return folder(arg);
    case VALIDFILE:
        return validfile(arg);
    case STRING:
        return TRUE;
    }
}

BOOL uinteger(char *arg)
{
    if (strcmp(arg, "0") == STRCMP_EQUAL)
        return TRUE;
    return strtoul(arg, NULL, 10) == 0 ? FALSE : TRUE;
}

BOOL folder(char *arg)
{
    BOOL res = FALSE;
    PDIRECTORY directory = opendir(arg);
    if(directory != NULL) {
        res = TRUE;
        closedir(directory);
    }
    return res;
}

BOOL validfile(char *arg)
{
    STAT st;
    if (stat(arg, &st) == -1)
    {
        logerror("cmdargs.c file(char* arg)");
        return FALSE;
    }
    return S_ISREG(st.st_mode);
}

void cleanErrorMessage() {
    if(errorMessage != NULL) {
        free(errorMessage);
    }
}