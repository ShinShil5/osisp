/**
    variant 2
    sync dirs - input: dir1, dir2; result - copy from dir1 to dir2 files that not exists in dir2
*/
#include "cdef.h"
#include "logger.h"
#include "cmdargs.h"
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>


#define STAT_ERROR -1

typedef struct dirent DIRECORY_ENTRY, *PDIRECORY_ENTRY;
typedef DIR DIRECTORY, *PDIRECTORY; 
typedef struct stat STAT, * PSTAT;

volatile int processesRunning = 0;
int processesMaxAmount = 0;

void copyFileProcess(char* src, char* dest);
void getShortName(char* fullName, char* buff);
int copyFile(char* srcFile, char* dstDir);
int openSrc(char* src);
int openDest(char* src, char* dest);
char* getFullPath(char* localPath);
void printResult(char* src, char* dest);

void syncDirs(char* dir1, char* dir2);
void syncFiles(PDIRECTORY dir, char* dir1, char* dir2);
BOOL fileExistsInDir(char* fullFileName, char* fileShortName, char* dir);
void startCopyProcess(char* fileName, char* dir);

void sigusr1Handler(int sig);
void main(int argc, char** argv) {
    char buff[32];
    sprintf(buff, "%d", getpid());
    setProgrammName(buff);
    setErrorCodesMode(FALSE);
    signal(SIGUSR1, sigusr1Handler);
    if(checkArgs(argc, argv, 3, folder, folder, uinteger) == FALSE) {        
        return;
    }
    processesMaxAmount = atoi(argv[3]);
    syncDirs(argv[1], argv[2]);
}

void waitForAllChildProcess() {
    int status = 0;
    pid_t wpid;
    while((wpid = wait(&status)) > 0);
}

void syncDirs(char* dir1, char* dir2) {
    PDIRECORY_ENTRY dirEnt;
    PDIRECTORY dir;
    dir = opendir(dir1);
    if(dir) {
        syncFiles(dir, dir1, dir2);    
    }else {
        logerror("Fail to open folder: %s", dir1);
    }
    closedir(dir);
}

void syncFiles(PDIRECTORY dir, char* dir1, char* dir2) {
    PDIRECORY_ENTRY dirEnt;
    while((dirEnt = readdir(dir))!=NULL) {        
        char fullFileName[1024];
        if(strcmp(dirEnt->d_name, ".") == STRCMP_EQUAL
        || strcmp(dirEnt->d_name, "..") == STRCMP_EQUAL) {
            continue;
        }
        strcpy(fullFileName, dir1);
        strcat(fullFileName, "/");
        strcat(fullFileName, dirEnt->d_name);
        STAT fileStatus = { 0 };
        if(stat(fullFileName, &fileStatus) != STAT_ERROR) {
            if(S_ISDIR(fileStatus.st_mode)) {
                char buff[1024];
                strcpy(buff, dir1);
                strcat(buff, "/");
                strcat(buff, dirEnt->d_name);
                syncDirs(buff, dir2);
            } else {
                if(fileExistsInDir(fullFileName, dirEnt->d_name, dir2) == FALSE) {                                                                            
                    while(processesRunning >= processesMaxAmount);
                    startCopyProcess(fullFileName, dir2);
                    ++processesRunning;
                }
            }
        }else {
            logerror("fail to copy: %s", fullFileName);                        
        }
        
    }
    waitForAllChildProcess();
}


void startCopyProcess(char* fullFileName, char* directoryToCopy) {
    int ret = fork();
    if(ret == 0) {
            char buff[32];
            sprintf(buff, "%d", getpid());
            setProgrammName(buff);
            copyFileProcess(fullFileName, directoryToCopy);
            kill(getppid(), SIGUSR1);
            _exit(0);
    } else if (ret < 0) {
            logerror("Fail to fork process");
    }
}

BOOL fileExistsInDir(char* fullFileName, char* fileName, char* dir) {
    PDIRECTORY directory;
    PDIRECORY_ENTRY dirEnt;
    BOOL res = FALSE;
    directory = opendir(dir);
    while((dirEnt = readdir(directory)) != NULL) {
        STAT fileStatus = { 0 };
        if(stat(fullFileName, &fileStatus) != STAT_ERROR) {
            if(strcmp(dirEnt->d_name, fileName) == STRCMP_EQUAL && !S_ISDIR(fileStatus.st_mode)) {
                res = TRUE;
                break;
            }
        }else {
            logerror("fail to copy %s", fullFileName);
        }
    }
    closedir(directory);
    return res;
}

void copyFileProcess(char* src, char* dest) {
    if(!copyFile(src, dest))
    {
        printResult(src, dest);     
    }
}

void printResult(char* src, char* dest) {
    char* fullDest = getFullPath(dest);
    logerror("%s %s", src, fullDest);
    free(fullDest);
}

void getShortName(char* fullName, char* buff) {
    int i, j;
    for(i = strlen(fullName) - 1; i>=0; --i) {
        if(fullName[i] == '\\' || fullName[i] == '/') {
            ++i;
            break;
        }
    }
    for(j = 0;i < strlen(fullName); ++i, ++j) {
        buff[j] = fullName[i];
    }
    buff[j] = '\0';
}

char* getFullPath(char* localPath) {
    char* res = (char*)malloc(sizeof(char) * 256);
    getcwd(res, 256);
    if(localPath[0] != '/') {
        strcat(res, "/");
    }
    strcat(res, localPath);
    return res;
}

int openSrc(char* src) {
    int res = open(src, O_RDONLY);
    if(res < 0) logerror("Fail to copy: %s", src);
    return res;
}
int openDest(char* src, char* dest) {
    char shortName[256];
    getShortName(src, shortName);
    char buffer[1024];
    strcpy(buffer, dest);
    if(dest[strlen(dest) - 1] != '/') {
        strcat(buffer, "/");
    }
    strcat(buffer, shortName);
    int res = open(buffer, O_CREAT | O_WRONLY);
    if(res < 0) {
        logerror("Fail to copy: %s", buffer);
    }
    return res;
}

int copyFile(char* srcFile, char* dstDir) {
    int src = openSrc(srcFile);
    int dest = openDest(srcFile, dstDir);
    if(src < 0 || dest < 0) return -1;
    char buffer[1024 * 16];
    int readenBytes = 0;
    while((readenBytes = read(src, buffer, sizeof(buffer))) > 0) {
        write(dest, buffer, readenBytes);
    }
    return 0;
}

void sigusr1Handler(int sig) {
    --processesRunning;
}