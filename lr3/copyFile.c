#include "cdef.h"
#include "logger.h"
#include "cmdargs.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <signal.h>

void getShortName(char* fullName, char* buff);
int copyFile(char* srcFile, char* dstDir);
int openSrc(char* src);
int openDest(char* src, char* dest);
char* getFullPath(char* localPath);
void printResult(char* src, char* dest);

void main(int argc, char** argv) {
    char buff[32];
    sprintf(buff, "%d", getpid());
    sleep(3);
    setProgrammName(buff);
    if(checkArgs(argc, argv, 2, VALIDFILE, FOLDER)) {
        if(!copyFile(argv[1], argv[2])) printResult(argv[1], argv[2]);     
    }    
    kill(getppid(), SIGUSR1);    
}

void printResult(char* src, char* dest) {
    char* fullDest = getFullPath(dest);
    char* fullSrc = getFullPath(src);
    logerror("%s %s", fullSrc, fullDest);
    free(fullDest);
    free(fullSrc);
}

void getShortName(char* fullName, char* buff) {
    int i, j;
    for(i = strlen(fullName) - 1; i>=0; --i) {
        if(fullName[i] == '\\' || fullName[i] == '/') {
            ++i;
            break;
        }
    }
    for(j = 0;i < strlen(fullName); ++i, ++j) {
        buff[j] = fullName[i];
    }
    buff[j] = '\0';
}

char* getFullPath(char* localPath) {
    char* res = (char*)malloc(sizeof(char) * 256);
    getcwd(res, 256);
    if(localPath[0] != '/') {
        strcat(res, "/");
    }
    strcat(res, localPath);
    return res;
}

int openSrc(char* src) {
    int res = open(src, O_RDONLY);
    if(res < 0) logerror("Fail to copy: %s", src);
    return res;
}
int openDest(char* src, char* dest) {
    char shortName[256];
    getShortName(src, shortName);
    char buffer[1024];
    strcpy(buffer, dest);
    if(dest[strlen(dest) - 1] != '/') {
        strcat(buffer, "/");
    }
    strcat(buffer, shortName);
    int res = open(buffer, O_CREAT | O_WRONLY);
    if(res < 0) {
        logerror("Fail to copy: %s", buffer);
    }
    return res;
}

int copyFile(char* srcFile, char* dstDir) {
    int src = openSrc(srcFile);
    int dest = openDest(srcFile, dstDir);
    if(src < 0 || dest < 0) return -1;
    char buffer[1024 * 16];
    int readenBytes = 0;
    while((readenBytes = read(src, buffer, sizeof(buffer))) > 0) {
        write(dest, buffer, readenBytes);
    }
    return 0;
}