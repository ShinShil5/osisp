#ifndef SHARE_MEMORY
#define SHARE_MEMORY

#include "cdef.h"
#include "logger.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <fcntl.h>

#define MUNMAP_ERROR -1

typedef struct _ShareMemBlock {
    char* shmName;
    void* block;
    int size;
} SHARE_MEM_BLOCK, * PSHARE_MEM_BLOCK;

PSHARE_MEM_BLOCK allocShareMem(char* mmapFileName, int size, BOOL create);
PSHARE_MEM_BLOCK getShareMem(char* mmapFileName, int size);
void freeShareMem(PSHARE_MEM_BLOCK block, BOOL destroyFile);

BOOL isFileNullSize(int mmapFd);
BOOL makeFileTheRightSize(int mmapFd, int size);

#endif