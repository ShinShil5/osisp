#include "shareMem.h"

/*
    umask - use in open, to set mask for execution permessions, open, ftruncate, lseek, write, mmap
*/

PSHARE_MEM_BLOCK allocShareMem(char* mmapFileName, int size, BOOL create) {
    void* retv = NULL;
    int flags = create == TRUE ? O_CREAT | O_RDWR : O_RDWR;
    int mmapFd = shm_open(mmapFileName, flags, S_IRUSR | S_IWUSR);
    if(mmapFd < 0) {
        logerror("fail shm_open, file - %s", mmapFileName);
        return NULL;
    }

    if(create == TRUE) {
        if(makeFileTheRightSize(mmapFd, size) == FALSE) {
          logerror("Can't make file the right size");
          return NULL;  
        } 
    }else {
        if(isFileNullSize(mmapFd) == TRUE)
        {
            logerror("File is null size");
            return NULL;
        } 
    }

    retv  =  mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, mmapFd, 0);

    if (retv == MAP_FAILED || retv == NULL)
    {               
        logerror("fail mmap");
        close(mmapFd);
        return NULL;
    }

    PSHARE_MEM_BLOCK res = (PSHARE_MEM_BLOCK)malloc(sizeof(SHARE_MEM_BLOCK));
    res->block = retv;
    res->size = size;
    res->shmName = (char*)malloc(sizeof(char) * (strlen(mmapFileName) + 1));
    strcpy(res->shmName, mmapFileName);
    close(mmapFd);
    return res;
}

BOOL isFileNullSize(int mmapFd) {
    BOOL res = FALSE;
    int result = lseek(mmapFd, 0, SEEK_END);         
    if (result == -1) 
    {  
        logerror("lseek mmapFd failed");   
        close(mmapFd);  
        res = TRUE;
    }  
    if (result == 0)  
    {  
        logerror("The file has 0 bytes");            
        close(mmapFd);  
        res = TRUE;
    }
    return res;
}

BOOL makeFileTheRightSize(int mmapFd, int size) {
    int res = TRUE;
    if((ftruncate(mmapFd, size) == 0)) {
        int result = lseek(mmapFd, size - 1, SEEK_SET);               
        if (result == -1)
        {               
            logerror("lseek mmapFd failed");
            close(mmapFd);
            res = FALSE;
        }else {
            result = write(mmapFd, "", 1);  
            if (result != 1)
            {               
                logerror("write mmapFd failed");
                close(mmapFd);
                res = FALSE;
            }
        }
    }
    return res;
}

void freeShareMem(PSHARE_MEM_BLOCK shareMem, BOOL destroyFile) {
    if(shareMem == NULL) return;
    if(shareMem->block == NULL || shareMem->size < 0) return;
    if(munmap(shareMem->block, shareMem->size) == MUNMAP_ERROR) {
        logerror("fail to unmap sharemem block");
        return;
    }
    if(shm_unlink(shareMem->shmName)) {
        logerror("fail to unlink sharemem, file name - %s", shareMem->shmName);
        return;
    }
    free(shareMem->shmName);
    free(shareMem);
}

/*

if(create == TRUE) {
        int mmapFd = shm_open(mmapFileName, O_CREAT|O_RDWR, S_IRUSR | S_IWUSR);      
        if (mmapFd < 0)   
        {                 
            logerror("open mmapFd failed");   
            return NULL;    
        }                 
        if ((ftruncate(mmapFd, size) == 0))               
        {                 
            int result = lseek(mmapFd, size - 1, SEEK_SET);               
            if (result == -1)
            {               
                logerror("lseek mmapFd failed");
                close(mmapFd);
                return NULL;
            }               
            // Something needs to be written at the end of the file to 
            // have the file actually have the new size.               
            // Just writing an empty string at the current file position will do.
            // Note:        
            //  - The current position in the file is at the end of the stretched
            //    file due to the call to lseek().  
            //  - The current position in the file is at the end of the stretched
            //    file due to the call to lseek(). 
            //  - An empty string is actually a single '\0' character, so a zero-byte   
            //    will be written at the last byte of the file.        
            
            result = write(mmapFd, "", 1);  
            if (result != 1)
            {               
                logerror("write mmapFd failed");
                close(mmapFd);
                return NULL;  
            }               
            retv  =  mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, mmapFd, 0);

            if (retv == MAP_FAILED || retv == NULL)
            {               
                logerror("mmap");
                close(mmapFd);
                return NULL;
            }
        }
    }
    else
    {  
        int mmapFd = shm_open(mmapFileName, O_RDWR, S_IRUSR | S_IWUSR);  
        if (mmapFd < 0)   
        {  
            logerror("fail to open: %s", mmapFileName);
            return NULL;    
        }
        int result = lseek(mmapFd, 0, SEEK_END);         
        if (result == -1) 
        {  
            logerror("lseek mmapFd failed");   
            close(mmapFd);  
            return NULL;    
        }  
        if (result == 0)  
        {  
            logerror("The file has 0 bytes");            
            close(mmapFd);  
            return NULL;    
        }              
        retv  =  mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, mmapFd, 0);         

        if (retv == MAP_FAILED || retv == NULL)  
        {                 
            logerror("mmap"); 
            close(mmapFd);  
            return NULL;    
        }                 
        close(mmapFd);    
    }

*/