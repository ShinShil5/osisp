#include "logger.h"

char* programmName = "";
BOOL memProgrammNameAlloc = FALSE;
BOOL errorMode = TRUE;
BOOL logMode = TRUE;
BOOL debugMode = TRUE;
BOOL errorCodesMode = TRUE;

const char* parseErrorCode(int errCode) {
    switch(errCode) {
        case 2:
            return "no such file or directory";
        case 3:
            return "no such process";
        case 8:
            return "exec format error";
        case 10:
            return "no child processes";
    }
    return "";
}

const char *getProgrammName()
{
    return programmName;
}

const char *setProgrammName(const char *name)
{
    if(memProgrammNameAlloc == TRUE) free(programmName);
    programmName = (char *)malloc(sizeof(char) * (strlen(name) + 1));
    memProgrammNameAlloc = TRUE;
    strcpy(programmName, name);
}

void freeLoggerMemory()
{
    free(programmName);
}

void logerror(const char *format, ...)
{
    if (errorOn() == TRUE) 
    {
        char buffer[1024];
        char res[1024];
        strcpy(res, "");
        if(programmNameValid() == TRUE) { 
            sprintf(buffer, "%s: ", programmName);
            strcat(res, buffer);
        }
        va_list args;
        va_start(args, format);
        vsprintf(buffer, format, args);        
        strcat(res, buffer);
        va_end(args);
        if (errno && errorCodesMode) {
            sprintf(buffer, ", errorCode - %d", errno);
            strcat(res, buffer);
            char descr[64];
            strcpy(descr, parseErrorCode(errno));
            if(strlen(descr) > 1) { 
                sprintf(buffer, ", %s", descr);
                strcat(res, buffer);
            }
        }
        fprintf(stderr, "%s\n", res);
    }
}

void logmsg(const char *format, ...)
{
    if (logOn() == TRUE)
    {
        va_list args;
        va_start(args, format);
        vprintf(format, args);
        va_end(args);
    }
}

void logdebug(const char* format, ...) {    
    if(debugOn() == TRUE) {
        fprintf(stdout, "[debug]");
        if(programmNameValid()) fprintf(stdout,"%s", programmName);
        fprintf(stdout, ": ");
        va_list args;
        va_start(args, format);    
        vfprintf(stdout, format, args);
        va_end(args);
        if (errno && errorCodesMode) fprintf(stdout, ", errorCode - %d, %s", errno, parseErrorCode(errno));
        fprintf(stdout, "\n");
    }
}

BOOL getErrorMode()
{
    return errorMode;
}
void setErrorMode(BOOL mode)
{
    errorMode = mode;
}
BOOL getLogMode()
{
    return logMode;
}
void setLogMode(BOOL mode)
{
    logMode = mode;
}
BOOL getDebugMode() {
    return debugMode;
}
void setDebugMode(BOOL mode) {
    debugMode = mode;
}
BOOL getErrorCodesMode() {
    return errorCodesMode;
}
void setErrorCodesMode(BOOL mode) {
    errorCodesMode = mode;
}
BOOL programmNameValid() {
    return strcmp(programmName, "") != STRCMP_EQUAL ? TRUE : FALSE;
}
BOOL logOn() {
    return logMode;
}
BOOL errorOn() {
    return errorMode;
}
BOOL debugOn() {
    return debugMode;
}
