#ifndef LOGGER_H
#define LOGGER_H

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "cdef.h"

extern char* programmName;
extern BOOL errorMode;
extern BOOL logMode;
extern BOOL debugMode;
extern BOOL errorCodesMode;

const char* getProgrammName();
const char* setProgrammName(const char* name);
BOOL getErrorMode();
void setErrorMode(BOOL errorMode);
BOOL getLogMode();
void setLogMode(BOOL logMode);
BOOL getDebugMode();
void setDebugMode(BOOL debugMode);
BOOL getErrorCodesMode();
void setErrorCodesMode(BOOL errorCodeMode);
void freeLoggerMemory();
const char* parseErrorCode(int errCode);


void logerror(const char* format, ...);
void logmsg(const char* format, ...);
void logdebug(const char* format, ...);

BOOL programmNameValid();
BOOL logOn();
BOOL errorOn();
BOOL debugOn();
#endif
