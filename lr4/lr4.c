/**
    variant 10
    1->2 2->3 3->(4,5,6) 6->7 4->8
    
    1->(8,7,6) SIGUSR1   8->4 SIGUSR1  7->4 SIGUSR2   
    6->4 SIGUSR1  4->(3,2) SIGUSR1 2->1 SIGUSR2

    1 A:8,7,6 B
    2 B:1 A
    3 - A
    4 A:3 B,A
    5 - -
    6 A:4 A
    7 B:4 A
    8 A:4 A

    console.log("Hello, world!");
*/

#include "lr4.h"

void main(int argc, char** argv) {
    setProgrammName("lr4.exe");  
    initGroups();  
    initContexts();    
    mapExec();
    replaceChildProcess(fork(), 1);
    while(getRunningAmount() != PROCESSES_AMOUNT);
    mapKills();    
    mapGroupKills();
    kill(contextByNum(1)->pid, contextByNum(1)->killSig);    
    waitForAllChildProcess();
    logmsg("%d %d %d Завершил работу после %d сигнала USR1 и %d сигнала USR2\n",contextByNum(1)->num, contextByNum(1)->pid, contextByNum(1)->ppid, contextByNum(1)->sigusr1Count, contextByNum(1)->sigusr2Count);
}