#ifndef RUNNNING_CONTEXT
#define RUNNNING_CONTEXT

#include "cdef.h"
#include "shareMem.h"
#include "logger.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <semaphore.h>
#include <signal.h>
#include <stdarg.h>
#include <sys/ipc.h>
#include <sys/wait.h>
#include <inttypes.h>
#include <time.h>
#include <math.h>

#define PROCESS_NAME_NOT_EXISTS NULL
#define PROCESS_ID_NOT_EXISTS -2
#define PROCESS_ID_NOT_SET -1
#define NON_ACTIVE 0
#define ACTIVE 1
#define PROCESSES_AMOUNT 8
#define MAX_FIRST_USR_AMOUNT 101
#define END_NUM_ENUMERATION 0
#define NAME_SIZE 16
#define REPLACE_NUMS_SIZE 4
#define PGID_PID_OF_CURR_PROCESS 0
#define PGID_SET_PGID_AS_PID 0
#define PROCESSES_IN_GROUP_MAX_AMOUNT 4
#define PROCESSES_GROUPS_AMOUNT 2
#define KILL_SIGNAL_TO_PGID_OF_THIS 0
#define TERMINATED 0
#define ALIVE 1

typedef struct _runningContextGroup {
    int amount;
    int processesNums[PROCESSES_IN_GROUP_MAX_AMOUNT];
    int mainProcessNum;
} RUNNING_CONTEXT_GROUP, * PRUNNING_CONTEXT_GROUP;

typedef struct _runningContext {
    int num;
    int killPgid;
    int active;
    int pid;
    int ppid;
    int pgid;
    char name[NAME_SIZE];
    int replaceAmount;
    int replaceNums[REPLACE_NUMS_SIZE];
    int killPid;
    int killSig;
    int sigusr1Count;
    int sigusr2Count;
    int currKillProcessesAmount;
    int needKillProcessesAmount;
    int terminated;
} RUNNING_CONTEXT, *PRUNNING_CONTEXT;

extern RUNNING_CONTEXT_GROUP contextsGroups[PROCESSES_GROUPS_AMOUNT];
extern RUNNING_CONTEXT contexts[PROCESSES_AMOUNT];
extern const char* semaphoreName;
extern int shmKey;
extern int shmId;
extern char* shm;
extern BOOL ready;
extern sem_t* sem;

void printContextInfo(int num);
void printContextsInfo();
void getArrField(int* arr, int size, char* buff);
int getRunningAmount();
void waitForAllChildProcess();
long getTimeMs();

//SETUP
void initGroups();
void initContexts();
void loadContexts();
void setupID(int num);
void setupPGID(int num);
void mapExec();
void mapKills();
void commonSetupContext(int index, int num);
void setNums(int num, ...);
void mapGroupKills();

//PROCESS
void commonProcess(int num);
void replaceChildProcess(int forkResult, int num);
void replaceChildProcesses(int num);
void commonProcessWait(int num);
void infiniteWait();
void usrHandler(int sig);
void commonProcessRec(int sig);
void commonProcessKill(int num, int sig);

//CONTROL
void setNextActiveContext(PRUNNING_CONTEXT pActiveContext);

//GETTERS
PRUNNING_CONTEXT contextByNum(int num);
PRUNNING_CONTEXT contextByPid(int pid);
char* getSigStr(int sig);
char* getProcessName(int num);


//CHECKERS
BOOL isReady();
BOOL isNumExists(int num);

#endif //RUNNING_CONTEXT    