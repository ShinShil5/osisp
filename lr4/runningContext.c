#include "runningContext.h"

RUNNING_CONTEXT contexts[PROCESSES_AMOUNT];
BOOL ready = FALSE;
int shmKey = 9875;
int shmId = 0;
char* shm = NULL;
const char* semaphoreName = "/usrHandlerSemaphore";
sem_t* sem = NULL;
RUNNING_CONTEXT_GROUP contextsGroups[PROCESSES_GROUPS_AMOUNT];

int sig1 = 2;
int sig2 = 1;

void initGroups() {
    RUNNING_CONTEXT_GROUP group0;
    group0.amount = 3;
    group0.processesNums[0] = 6;
    group0.processesNums[1] = 7;
    group0.processesNums[2] = 8;
    group0.mainProcessNum = 6;
    contextsGroups[0] = group0;

    RUNNING_CONTEXT_GROUP group1;
    group1.amount = 2;
    group1.processesNums[0] = 3;
    group1.processesNums[1] = 2;
    group1.mainProcessNum = 2;
    contextsGroups[1] = group1;    
}

void initContexts() {
    shmId = shmget(shmKey, sizeof(contexts), IPC_CREAT | 0666);
    if(shmId < 0) {
        logerror("Fatal! Can't shmget creat");
        exit(1);
    }
    shm = shmat(shmId, NULL, 0);
    if(shm == (char*)-1) {
        logerror("Fatal! Can't shmat");
        exit(1);
    }
    memset(shm, 0, sizeof(contexts));
    for(int i = 0; i<PROCESSES_AMOUNT; ++i) {
        commonSetupContext(i, i + 1);
    }
    sem = sem_open(semaphoreName, O_CREAT, 0777, 0);
    sem_init(sem, 10, 1);
}

void loadContexts() {
    shmId = shmget(shmKey, sizeof(contexts), 0666);
    if(shmId < 0) {
        logerror("Fatal! Can't shmget load");
        exit(1);
    }
    shm = shmat(shmId, NULL, 0);
    if(shm == (char*)-1) {
        logerror("Fatal! Can't shmat");
        exit(1);
    }
}

void freeContexts() {

}

void setupID(int num) {
    if(isNumExists(num) == FALSE) {
        logerror("Num not exists, custom error");        
        return;
    }
    contextByNum(num)->pid = getpid();
    contextByNum(num)->ppid = getppid();
    setpgid(PGID_PID_OF_CURR_PROCESS, PGID_SET_PGID_AS_PID);
}

void setupPGID(int num) {
    if(num == 6 || num == 7 || num == 8) {
        setpgid(getpid(), contextByNum(6)->pid);        
    } else if (num == 2 || num == 3) {
        setpgid(getpid(), contextByNum(2)->pid);
    } 
    contextByNum(num)->pgid = getpgid(getpid());
}

void setNums(int num, ...) {
    PRUNNING_CONTEXT pContext = contextByNum(num);
    va_list nums;
    va_start(nums, num);
    int nextNum = END_NUM_ENUMERATION + 1;
    int amount = 0;
    while((nextNum = va_arg(nums, int)) != END_NUM_ENUMERATION) {
        pContext->replaceNums[amount++] = nextNum;                
    }
    pContext->replaceAmount = amount;
    va_end(nums);
}

void mapExec() {
    setNums(1, 2, END_NUM_ENUMERATION);
    setNums(2, 3, END_NUM_ENUMERATION);
    setNums(3, 4, 5, 6, END_NUM_ENUMERATION);    
    setNums(6, 7, END_NUM_ENUMERATION);
    setNums(4, 8, END_NUM_ENUMERATION);
}

void mapKills() {
    contextByNum(1)->killPid = KILL_SIGNAL_TO_PGID_OF_THIS;
    contextByNum(1)->killSig = SIGUSR1;

    contextByNum(8)->killPid = contextByNum(4)->pid;
    contextByNum(8)->killSig = SIGUSR1;

    contextByNum(7)->killPid = contextByNum(4)->pid;
    contextByNum(7)->killSig = SIGUSR2;

    contextByNum(6)->killPid = contextByNum(4)->pid;
    contextByNum(6)->killSig = SIGUSR1;

    contextByNum(4)->killPid = KILL_SIGNAL_TO_PGID_OF_THIS;
    contextByNum(4)->killSig = SIGUSR1;

    contextByNum(2)->killPid = contextByNum(1)->pid;
    contextByNum(2)->killSig = SIGUSR2;

    sleep(6);
}

void mapGroupKills() {
    contextByNum(1)->killPgid = contextByNum(6)->pgid;
    contextByNum(4)->killPgid = contextByNum(2)->pgid;

}

BOOL isNumExists(int num) {
    return num < 1 || num > PROCESSES_AMOUNT ? FALSE : TRUE;
}

PRUNNING_CONTEXT contextByPid(int pid) {
    for(int i = 0; i<PROCESSES_AMOUNT; ++i) {
        if(((PRUNNING_CONTEXT)(shm + (sizeof(RUNNING_CONTEXT) * i)))->pid == pid) {
            return (PRUNNING_CONTEXT)(shm + (sizeof(RUNNING_CONTEXT) * i));
        }
    }
    return NULL;
}

PRUNNING_CONTEXT contextByNum(int num) {
    if(isNumExists(num) == FALSE) {
        logerror("Num '%d' not exists, custom error", num);
        return NULL;
    }
    for(int i = 0; i<PROCESSES_AMOUNT; ++i) {
        if(((PRUNNING_CONTEXT)(shm + (sizeof(RUNNING_CONTEXT) * i)))->num == num) {
            return (PRUNNING_CONTEXT)(shm + (sizeof(RUNNING_CONTEXT) * i));
        }
    }
    return NULL;
}

void commonSetupContext(int index, int num) {
    PRUNNING_CONTEXT pContext = (PRUNNING_CONTEXT)(shm + (sizeof(RUNNING_CONTEXT) * index));
    strcpy(pContext->name, getProcessName(num));
    pContext->num = num;
    pContext->pid = PROCESS_ID_NOT_SET;
    pContext->active = NON_ACTIVE;
    pContext->replaceAmount = 0;
    pContext->sigusr1Count = 0;
    pContext->sigusr2Count = 0;
    pContext->terminated = ALIVE;
}

void setNextActiveContext(PRUNNING_CONTEXT pActiveContext) {
    pActiveContext->active = NON_ACTIVE;
    contextByNum(pActiveContext->killPgid)->active = ACTIVE;
}

void sigtermHandler(int sig) {
    PRUNNING_CONTEXT pContext = contextByPid(getpid());
    logmsg("%d %d %d Завершил работу после %d сигнала USR1 и %d сигнала USR2\n",pContext->num, pContext->pid, pContext->ppid, pContext->sigusr1Count, pContext->sigusr2Count);
    waitForAllChildProcess();
    _exit(0);
}

void usrHandler1(int sig)  {
    sem_wait(sem);
    PRUNNING_CONTEXT pContext = contextByPid(getpid());    
    if(pContext->terminated == TERMINATED) {
        sem_post(sem);
        _exit(0);
    }
    commonProcessRec(sig);

    if(pContext->num == 1) {
        if(pContext->sigusr1Count + pContext->sigusr2Count > MAX_FIRST_USR_AMOUNT) {
            for(int i = 2; i<=PROCESSES_AMOUNT; ++i) {
                contextByNum(i)->terminated = TERMINATED;    
                kill(contextByNum(i)->pid, SIGTERM);
            }
            sem_post(sem);
            waitForAllChildProcess();
            _exit(0);
        }
    }

    if(sig == SIGUSR1) pContext->sigusr1Count += 1;
    if(sig == SIGUSR2) pContext->sigusr2Count += 1;
    
    commonProcessKill(pContext->killPid, pContext->killSig); 
    sem_post(sem);
}

void commonProcessRec(int sig) {        
    printf("%d %4d %4d RECV %s %ld\n", contextByPid(getpid())->num, getpid(), getppid(), getSigStr(sig), getTimeMs());    
}

void commonProcessKill(int pid, int sig) {
    if(getSigStr(sig) != NULL) {        
        if(contextByPid(getpid())->num == 4) {
            logerror("4");
            if(sig1 == 0 && sig2 == 0) {
                printf("%d %4d %4d KILL %s %ld\n", contextByPid(getpid())->num, getpid(), getppid(), getSigStr(sig), getTimeMs());    
                killpg(contextByPid(getpid())->killPgid,sig);        
                sig1 = 2;
                sig2 = 1;
            } else if(sig == SIGUSR1) {
                --sig1;
            } else if (sig == SIGUSR2) {
                --sig2;
            }
        } else {
            printf("%d %4d %4d KILL %s %ld\n", contextByPid(getpid())->num, getpid(), getppid(), getSigStr(sig), getTimeMs());    
            if(pid == KILL_SIGNAL_TO_PGID_OF_THIS) {
                logerror("WHAT TAK SKUCHAI");
                killpg(contextByPid(getpid())->pgid, sig);
            }else {
                kill(pid, sig);
            }
        }
    }
}

long getTimeMs() {
    long            ms; // Milliseconds
    time_t          s;  // Seconds
    struct timespec spec;
    clock_gettime(CLOCK_REALTIME, &spec);

    s  = spec.tv_sec;
    ms = round(spec.tv_nsec / 1.0e3);

    return ms;
}

char *getProcessName(int num)
{
    if (num < 1 || num > 8)
    {
        return PROCESS_NAME_NOT_EXISTS;
    }
    char* res = (char*)malloc(sizeof(char) * 16);
    sprintf(res, "%d", num);
    return res;
}

void setReady() {
    ready = TRUE;
}

BOOL getReady() {
    return ready;
}

void processCommonPart(int num) {
    setProgrammName(getProcessName(num));    
    loadContexts();
    signal(SIGUSR1, usrHandler);
    signal(SIGUSR2, usrHandler);
    signal(SIGTERM, sigtermHandler);
    setupID(num);
    replaceChildProcesses(num);
    sleep(5);
    setupPGID(num);
}

void commonProcess(int num) {
    processCommonPart(num);
    infiniteWait();
}
void replaceChildProcesses(int num) {
    for(int i = 0; i<contextByNum(num)->replaceAmount; ++i) {
        replaceChildProcess(fork(), contextByNum(num)->replaceNums[i]);
        sleep(1);
    }
}

void infiniteWait() {
    while(TRUE == TRUE);
}

void replaceChildProcess(int forkResult, int num) {
    switch(forkResult) {
        case FORK_RESULT_CHILD: 
            if (errno) logerror("Fail to fork process with num: %d", num);
            setProgrammName(getProcessName(num));
            commonProcess(num);
            break;
        case FORK_RESULT_ERROR: 
            logerror("Fail to fork process with num: %d", num);
            break;
        default:
            break;
    }
}

void printContextsInfo() {
    printf("%4s %4s %4s %s", "Num", "PID", "PGID", "Kill map");
    char buffer[32];
    for(int i = 0; i<PROCESSES_AMOUNT; ++i) {
        PRUNNING_CONTEXT pContext = (PRUNNING_CONTEXT)(shm + (sizeof(RUNNING_CONTEXT) * i));
        sprintf(buffer, "%s -> killId: %d, killPgid: %d", getSigStr(pContext->killSig), pContext->killPid, pContext->killPgid);
        printf("\n%4d %4d %4d %s", pContext->num, pContext->pid, pContext->pgid, buffer);
    }
    
}

void printContextInfo(int num) {
    PRUNNING_CONTEXT context = contextByNum(num);
    char* format = "[ContextInfo]\n\tnum: %d\n\tkillPgid: %d\n\tactive: %d\n\tpid: %d\n\tppid: %d\n\tpgid: %d\n\tname: %s\n\treplaceAmount: %d\n\treplaceNums: %s\n\tkillPid: %d\n\tkillSig: %d\n\tsigusr1Count: %d\n\tsigusr2Count: %d\n\tcurrKillProcessesAmount: %d\n\tneedKillProcessesAmount: %d";
    char replaceNums[256];
    getArrField(context->replaceNums, context->replaceAmount, replaceNums);
    logerror(format, 
            context->num, 
            context->killPgid, 
            context->active, 
            context->pid, 
            context->ppid, 
            context->pgid, 
            context->name, 
            context->replaceAmount, 
            replaceNums, 
            context->killPid, 
            context->killSig, 
            context->sigusr1Count, 
            context->sigusr2Count, 
            context->currKillProcessesAmount, 
            context->needKillProcessesAmount);
}

void getArrField(int* arr, int size, char* buff) {    
    if(size <= 0) return; 
    char localBuff[256];
    sprintf(buff, "%d", arr[0]);
    for(int i = 1; i<size; ++i) {
        sprintf(localBuff, ", %d", arr[i]);
        strcat(buff, localBuff);
    }    
}

int getRunningAmount() {
    int res = 0;
    for(int i = 0; i<PROCESSES_AMOUNT; ++i) {
        if(contextByNum(i+1)->pid != PROCESS_ID_NOT_SET) res++;
    }
    return res;
}

char* getSigStr(int sig)  {
    switch(sig) {
        case SIGUSR1:
            return "SIGUSR1";
        case SIGUSR2:
            return "SIGUSR2";
        case SIGTERM:
            return "SIGTERM";
        default:
            return NULL;
    }
}

void waitForAllChildProcess() {
    int status = 0;
    pid_t wpid;
    while((wpid = wait(&status)) > 0);
}

// PRUNNING_CONTEXT pContext = contextByPid(getpid());        
//     PRUNNING_CONTEXT pContextParent = contextByPid(getppid());  
//     int parentNum = pContextParent ? pContextParent->num : 0;
// PRUNNING_CONTEXT pContext = contextByPid(getpid());        
//         PRUNNING_CONTEXT pContextParent = contextByPid(getppid());  
//         int parentNum = pContextParent ? pContextParent->num : 0;