#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <locale.h>

#ifdef HAVE_ST_BIRTHTIME
#define birthtime(x) x.st_birthtime
#else
#define birthtime(x) x.st_ctime
#endif

#define ERROR 5
#define FOUND 4
#define NOT_FOUND 3
#define STRINGS_EQUAL 0
#define STAT_ERROR -1
#define FALSE 0
#define TRUE 1

typedef struct dirent DIRECTORY_ENTRY, *PDIRECTORY_ENTRY;
typedef DIR DIRECTORY, *PDIRECTORY; 
typedef struct stat STAT, * PSTAT;
typedef struct _resFindFile {
    int fileAmount;
    int folderAmount;
    int retCode;
    int findAmount;
    char** fileName;
} RES_FIND_FILE, * PRES_FIND_FILE;