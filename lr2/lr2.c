
#include "cdef.h"
#include "lr2.h"
#include "logger.h"
#include "cmdargs.h"

void findFile(char* dirName, char* fileName, PRES_FIND_FILE res);
void printResult(PRES_FIND_FILE res);
int defStat(char* pathName, PSTAT fileStatus);
PRES_FIND_FILE init();

void main(int argc, char** argv) {
    setProgrammName("lr2.exe");
    if(!checkArgs(argc, argv, 2, FOLDER, STRING)) {
        logerror("Usage: ./lr2 [existing folder] [file for search]");
        return;
    }
    PRES_FIND_FILE res = init();
    findFile(argv[1], argv[2], res);
    printResult(res);
}

PRES_FIND_FILE init() {
    setlocale(LC_TIME, "ru_RU.utf8");
    PRES_FIND_FILE res = (PRES_FIND_FILE)malloc(sizeof(RES_FIND_FILE));
    res->fileName = (char**)malloc(sizeof(char*)*8);
    for(int i = 0; i<8; ++i) {
        res->fileName[i] = (char*)malloc(sizeof(char) * 256);
    }
    res->fileAmount = 0;
    res->folderAmount = 0;
    res->retCode = 0;
    res->findAmount = 0;
    return res;
}


/*
001777
*/
void printResult(PRES_FIND_FILE res) {
    if(res->findAmount > 0) {
        for(int i = 0; i<  res->findAmount; ++i) {
            STAT fileDescription;
            int resStat = stat(res->fileName[i], &fileDescription);
            if(resStat) {
                logerror("Error in printResult");
                return;
            }        
            logmsg("%s", res->fileName[i]);        
            logmsg(" %lld", (long long)fileDescription.st_size);
            time_t birth = birthtime(fileDescription);
            char* tmp = (char*)malloc(sizeof(char)*1024);
            struct tm * tmBirth = localtime(&birth);
            strftime(tmp, 1024, " %a - %d/%b/%y", tmBirth);
            logmsg(" %s", tmp);
            logmsg(" %lo", (unsigned long)fileDescription.st_mode);
            logmsg(" %ld\n", (long)fileDescription.st_ino);
            free(tmp);
        }
    }else {
        logmsg("0\n");        
    }
    logmsg(" %d", res->fileAmount);
    logmsg(" %d", res->folderAmount);
    logmsg(" %d", res->findAmount);
    logmsg("\n\n");
}

void findFile(char* dirName, char* fileName, PRES_FIND_FILE res) {    
    char pathName[PATH_MAX + 1];
    PDIRECTORY pDirectory;
    PDIRECTORY_ENTRY pDirectoryEntry;
    pDirectory = opendir(dirName);
    if(pDirectory == NULL) {
        logerror("Error opening directory: %s", dirName);
        return;
    }
    pDirectoryEntry = readdir(pDirectory);
    while(pDirectoryEntry != NULL) {
        if( ( strcmp( pDirectoryEntry->d_name, "." ) == STRCMP_EQUAL ) ||
            ( strcmp( pDirectoryEntry->d_name, ".." ) == STRCMP_EQUAL ) ) {
            pDirectoryEntry = readdir(pDirectory);
            continue;
        }
        strcpy( pathName, dirName );
        strcat( pathName, "/");
        strcat( pathName, pDirectoryEntry->d_name);
        if(strcmp(pDirectoryEntry->d_name, fileName) == STRCMP_EQUAL) {
            ++res->fileAmount;
            strcpy( res->fileName[res->findAmount], pathName );
            ++res->findAmount;
            pDirectoryEntry = readdir(pDirectory);
            continue;
        }
        else {
            STAT fileStatus = { 0 };
            if(defStat(pathName, &fileStatus) == -1) {
                ++res->fileAmount;        
            }
            else if(S_ISDIR(fileStatus.st_mode)) {
                findFile(pathName, fileName, res);     
                ++res->folderAmount;          
            } else {
                ++res->fileAmount;
            }
            pDirectoryEntry = readdir(pDirectory);
        }
    }
    closedir(pDirectory);
}

int defStat(char* pathName, PSTAT fileStatus) {
    char* resPath = (char*)malloc(sizeof(char)*257);
    if(lstat(pathName, fileStatus) == STAT_ERROR) {
        if(S_ISLNK(fileStatus->st_mode)) {
            if(realpath(pathName, resPath) == NULL) {
                logerror("error realpath for link: %s", errno);
                free(resPath);
                return STAT_ERROR;
            }
            if(stat(resPath, fileStatus) == STAT_ERROR) {
                logerror("error stat for file: %s", resPath);
                free(resPath);
                return STAT_ERROR;
            }
        } else {
            logerror("error stat for file: %s", resPath);        
            return STAT_ERROR;
        }
    }
    free(resPath);
    return 0;
}