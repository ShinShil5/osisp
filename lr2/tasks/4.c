#include <stdio.h>
#include <stdlib.h>

void main(int argc, char** argv) {
    int n = atoi(argv[2]);
    int nCounter = 0;
    FILE* file = fopen(argv[1], "r");
    if(!file) {
        printf("Error opening the file: %s\n", argv[1]);
    }else {
        while(!feof(file)) {
            for(char ch=getc(file); !feof(file) && ch!='\n'; ch=getc(file)) {
                putc(ch, stdout);
            }
            if(feof(file)) {
                break;
            }
            putc('\n', stdout);
            ++nCounter;
            if(nCounter == n) {
                printf("Press enter to continue");
                getc(stdin);
                nCounter = 0;
            }
        }
    }
}