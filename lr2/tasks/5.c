#include <stdio.h>
#include <stdlib.h>

void main(int argc, char** argv) {
    FILE* input = fopen(argv[1], "r");
    if(!input) {
        printf("Error opening the file: %s\n", argv[1]);
        return;
    }
    FILE* output = fopen(argv[2], "w");
    if(!output) {
        printf("Error opening the file: %s\n", argv[2]);
        return;
    }
    for(; !feof(input); putc(getc(input),output));
    fclose(input);
    fclose(output);
    printf("Succesfully copied from %s to %s\n", argv[1], argv[2]);
}