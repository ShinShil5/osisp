#include <stdio.h>

void main(int argc, char** argv) {
    FILE* file = fopen(argv[1], "w");
    if(!file) {
        printf("Error opening the file: %s\n", argv[1]);
    }else {
        printf("Print the string, press ctrl+F to stop: ");
        for(char ch = getc(stdin); ch!=6; ch=getc(stdin)) {
            putc(ch, file);
        }
        fclose(file);
        printf("The string hass been succesfully written to: %s\n", argv[1]);
    }
}