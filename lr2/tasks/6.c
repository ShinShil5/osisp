#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>

const int PATH_MAX = 255;

typedef struct dirent DIRENT,* PDIRENT;
typedef DIR* PDIR;
typedef struct stat STAT;

unsigned printDirContent(char* dirName) {
    PDIR pdir = NULL;
    PDIRENT pentry = NULL;
    int retval = 0;
    char pathName[PATH_MAX + 1];
    pdir = opendir(dirName);
    if(pdir == NULL) {
        printf("Error opening directory: %s, errcode: %d", dirName, errno);
        return retval;
    }
    pentry = readdir(pdir);
    while(pentry != NULL) {
        if( ( strcmp( pentry->d_name, "." ) == 0 ) ||
            ( strcmp( pentry->d_name, ".." ) == 0 ) ) {
            pentry = readdir(pdir);
            continue;
        }
        strcpy( pathName, dirName );
        strcat( pathName, "/");
        strcat( pathName, pentry->d_name);
        printf("%s\n", pathName);
        pentry = readdir(pdir);        
    }
    closedir(pdir);
    return retval;
}

void main(int argc, char** argv) {
    char currDir[PATH_MAX + 1];
    getcwd(currDir, PATH_MAX);
    printf("\n%s\n", currDir);
    printDirContent(currDir);
    printf("\nROOT\n");
    printDirContent("/");
    printf("\n\n");
}